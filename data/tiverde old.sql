-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Jun-2019 às 10:49
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiverde`
--
CREATE DATABASE IF NOT EXISTS `tiverde` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tiverde`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`) VALUES
(3, 'Informatica'),
(4, 'Eletrônicos'),
(5, 'Teste'),
(6, 'Teste'),
(7, 'Reteste'),
(8, 'Ir na graxaria');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidades`
--

CREATE TABLE `cidades` (
  `id` int(2) NOT NULL,
  `nome` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `sigla` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cidades`
--

INSERT INTO `cidades` (`id`, `nome`, `sigla`) VALUES
(445, 'Abreulândia', 'TO'),
(446, 'Aguiarnópolis', 'TO'),
(447, 'Aliança do Tocantins', 'TO'),
(448, 'Almas', 'TO'),
(449, 'Alvorada', 'TO'),
(450, 'Ananás', 'TO'),
(451, 'Angico', 'TO'),
(452, 'Aparecida do Rio Negro', 'TO'),
(453, 'Aragominas', 'TO'),
(454, 'Araguacema', 'TO'),
(455, 'Araguaçu', 'TO'),
(456, 'Araguaína', 'TO'),
(457, 'Araguanã', 'TO'),
(458, 'Araguatins', 'TO'),
(459, 'Arapoema', 'TO'),
(460, 'Arraias', 'TO'),
(461, 'Augustinópolis', 'TO'),
(462, 'Aurora do Tocantins', 'TO'),
(463, 'Axixá do Tocantins', 'TO'),
(464, 'Babaçulândia', 'TO'),
(465, 'Bandeirantes do Tocantins', 'TO'),
(466, 'Barra do Ouro', 'TO'),
(467, 'Barrolândia', 'TO'),
(468, 'Bernardo Sayão', 'TO'),
(469, 'Bom Jesus do Tocantins', 'TO'),
(470, 'Brasilândia do Tocantins', 'TO'),
(471, 'Brejinho de Nazaré', 'TO'),
(472, 'Buriti do Tocantins', 'TO'),
(473, 'Cachoeirinha', 'TO'),
(474, 'Campos Lindos', 'TO'),
(475, 'Cariri do Tocantins', 'TO'),
(476, 'Carmolândia', 'TO'),
(477, 'Carrasco Bonito', 'TO'),
(478, 'Caseara', 'TO'),
(479, 'Centenário', 'TO'),
(480, 'Chapada da Natividade', 'TO'),
(481, 'Chapada de Areia', 'TO'),
(482, 'Colinas do Tocantins', 'TO'),
(483, 'Colméia', 'TO'),
(484, 'Combinado', 'TO'),
(485, 'Conceição do Tocantins', 'TO'),
(486, 'Couto de Magalhães', 'TO'),
(487, 'Cristalândia', 'TO'),
(488, 'Crixás do Tocantins', 'TO'),
(489, 'Darcinópolis', 'TO'),
(490, 'Dianópolis', 'TO'),
(491, 'Divinópolis do Tocantins', 'TO'),
(492, 'Dois Irmãos do Tocantins', 'TO'),
(493, 'Dueré', 'TO'),
(494, 'Esperantina', 'TO'),
(495, 'Fátima', 'TO'),
(496, 'Figueirópolis', 'TO'),
(497, 'Filadélfia', 'TO'),
(498, 'Formoso do Araguaia', 'TO'),
(499, 'Fortaleza do Tabocão', 'TO'),
(500, 'Goianorte', 'TO'),
(501, 'Goiatins', 'TO'),
(502, 'Guaraí', 'TO'),
(503, 'Gurupi', 'TO'),
(504, 'Ipueiras', 'TO'),
(505, 'Itacajá', 'TO'),
(506, 'Itaguatins', 'TO'),
(507, 'Itapiratins', 'TO'),
(508, 'Itaporã do Tocantins', 'TO'),
(509, 'Jaú do Tocantins', 'TO'),
(510, 'Juarina', 'TO'),
(511, 'Lagoa da Confusão', 'TO'),
(512, 'Lagoa do Tocantins', 'TO'),
(513, 'Lajeado', 'TO'),
(514, 'Lavandeira', 'TO'),
(515, 'Lizarda', 'TO'),
(516, 'Luzinópolis', 'TO'),
(517, 'Marianópolis do Tocantins', 'TO'),
(518, 'Mateiros', 'TO'),
(519, 'Maurilândia do Tocantins', 'TO'),
(520, 'Miracema do Tocantins', 'TO'),
(521, 'Miranorte', 'TO'),
(522, 'Monte do Carmo', 'TO'),
(523, 'Monte Santo do Tocantins', 'TO'),
(524, 'Muricilândia', 'TO'),
(525, 'Natividade', 'TO'),
(526, 'Nazaré', 'TO'),
(527, 'Nova Olinda', 'TO'),
(528, 'Nova Rosalândia', 'TO'),
(529, 'Novo Acordo', 'TO'),
(530, 'Novo Alegre', 'TO'),
(531, 'Novo Jardim', 'TO'),
(532, 'Oliveira de Fátima', 'TO'),
(533, 'Palmas', 'TO'),
(534, 'Palmeirante', 'TO'),
(535, 'Palmeiras do Tocantins', 'TO'),
(536, 'Palmeirópolis', 'TO'),
(537, 'Paraíso do Tocantins', 'TO'),
(538, 'Paranã', 'TO'),
(539, 'Pau d`Arco', 'TO'),
(540, 'Pedro Afonso', 'TO'),
(541, 'Peixe', 'TO'),
(542, 'Pequizeiro', 'TO'),
(543, 'Pindorama do Tocantins', 'TO'),
(544, 'Piraquê', 'TO'),
(545, 'Pium', 'TO'),
(546, 'Ponte Alta do Bom Jesus', 'TO'),
(547, 'Ponte Alta do Tocantins', 'TO'),
(548, 'Porto Alegre do Tocantins', 'TO'),
(549, 'Porto Nacional', 'TO'),
(550, 'Praia Norte', 'TO'),
(551, 'Presidente Kennedy', 'TO'),
(552, 'Pugmil', 'TO'),
(553, 'Recursolândia', 'TO'),
(554, 'Riachinho', 'TO'),
(555, 'Rio da Conceição', 'TO'),
(556, 'Rio dos Bois', 'TO'),
(557, 'Rio Sono', 'TO'),
(558, 'Sampaio', 'TO'),
(559, 'Sandolândia', 'TO'),
(560, 'Santa Fé do Araguaia', 'TO'),
(561, 'Santa Maria do Tocantins', 'TO'),
(562, 'Santa Rita do Tocantins', 'TO'),
(563, 'Santa Rosa do Tocantins', 'TO'),
(564, 'Santa Tereza do Tocantins', 'TO'),
(565, 'Santa Terezinha do Tocantins', 'TO'),
(566, 'São Bento do Tocantins', 'TO'),
(567, 'São Félix do Tocantins', 'TO'),
(568, 'São Miguel do Tocantins', 'TO'),
(569, 'São Salvador do Tocantins', 'TO'),
(570, 'São Sebastião do Tocantins', 'TO'),
(571, 'São Valério da Natividade', 'TO'),
(572, 'Silvanópolis', 'TO'),
(573, 'Sítio Novo do Tocantins', 'TO'),
(574, 'Sucupira', 'TO'),
(575, 'Taguatinga', 'TO'),
(576, 'Taipas do Tocantins', 'TO'),
(577, 'Talismã', 'TO'),
(578, 'Tocantínia', 'TO'),
(579, 'Tocantinópolis', 'TO'),
(580, 'Tupirama', 'TO'),
(581, 'Tupiratins', 'TO'),
(582, 'Wanderlândia', 'TO'),
(583, 'Xambioá', 'TO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id` int(11) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `telefone`, `email`) VALUES
(75, '63992006705', 'lucianoguida10@gmail.com'),
(76, '6333613493', 'dorieneguida@gmail.com'),
(77, '6333613493', 'dorieneguida@gmail.com'),
(78, '6333613493', 'dorieneguida@gmail.com'),
(79, '6333613493', 'dorieneguida@gmail.com'),
(81, '63992006705', 'numquerosaber@gmail.com'),
(82, '63992006705', 'numquerosaber@gmail.com'),
(83, '6333613493', 'dorieneguida@gmail.com'),
(84, '(63) 99200-6705', 'lucianoguida10@gmail.com'),
(85, '(63) 99200-6705', 'lucianoguida10@gmail.com'),
(86, '(63) 99200-6705', 'lucianoguida10@gmail.com'),
(87, '(63) 99200-6705', 'lucianoguida10@gmail.com'),
(94, '(63) 99200-6705', 'lucianoguida10@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `descartes`
--

CREATE TABLE `descartes` (
  `id` int(11) NOT NULL,
  `material` varchar(45) NOT NULL,
  `descricao` longtext NOT NULL,
  `img` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  `coletor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `descartes`
--

INSERT INTO `descartes` (`id`, `material`, `descricao`, `img`, `status`, `data`, `user_id`, `categoria_id`, `endereco_id`, `coletor_id`) VALUES
(17, 'Mouses 3', 'adasdasdas', 'upload/7b521ef15f77ff7e9f8067e03b2309a5.png', '1', '2019-06-08 11:46:03', 73, 4, 56, NULL),
(18, 'Mouses 3', 'asdasda', 'upload/570f1d6b0fb3e1eebdffc16657566761.png', '1', '2019-06-08 11:46:17', 73, 4, 56, NULL),
(19, 'Mouses', 'adsdasdasdasd ', 'upload/c465eb6908356e45b35cb4a815474642.png', '1', '2019-06-08 11:46:26', 73, 3, 56, NULL),
(29, 'Mouses 3', 'asdasdas', 'upload/dea2910495db9abd9f37943ae5059ce4.jpg', '3', '2019-06-11 13:53:37', 83, 3, 66, 73),
(30, 'Mouses 3', 'adasd', 'upload/7d91ccf88acde77b76d20d003da5aaba.jpg', '1', '2019-06-11 14:46:24', 83, 4, 66, NULL),
(31, 'Mouses', 'gdgdfg', 'upload/6ddfe6a1e37a2d93be8d0ddc16176a9d.jpg', '1', '2019-06-11 15:15:54', 83, 5, 66, NULL),
(32, 'Mouses 3', 'iii', 'upload/6399d5af2ad6ede01c832fe00c5f5df8.jpg', '1', '2019-06-11 15:16:30', 83, 8, 66, NULL),
(33, 'Mouses 3', 'asdas', 'upload/585815201595061445c21d86ec613e3d.jpg', '1', '2019-06-11 15:44:38', 83, 4, 66, NULL),
(34, 'Mouses 3', '333', 'upload/bb551c92985d5deb78b279eca754789c.jpg', '1', '2019-06-11 15:49:21', 83, 5, 66, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `enderecos`
--

CREATE TABLE `enderecos` (
  `id` int(11) NOT NULL,
  `rua` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `cidade_id` int(2) NOT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cep` int(45) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `principal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `enderecos`
--

INSERT INTO `enderecos` (`id`, `rua`, `numero`, `cidade_id`, `bairro`, `cep`, `lat`, `lng`, `user_id`, `principal`) VALUES
(56, 'Jose nezio ramos', '949', 454, 'Alto paraiso', 77600000, '-10.020274', '-48.5957174', 73, 1),
(66, NULL, NULL, 452, NULL, 0, '-10.1832994', '-48.3372365', 83, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `IP` varchar(20) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lost_passwords`
--

CREATE TABLE `lost_passwords` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `IP` varchar(20) NOT NULL,
  `token` char(128) NOT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `minhascategorias`
--

CREATE TABLE `minhascategorias` (
  `user_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `minhascategorias`
--

INSERT INTO `minhascategorias` (`user_id`, `categoria_id`) VALUES
(73, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `data_nascimento` varchar(255) DEFAULT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `obs` mediumtext,
  `role` varchar(45) NOT NULL,
  `status` int(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `contato_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `nome`, `cpf`, `data_nascimento`, `password`, `salt`, `obs`, `role`, `status`, `token`, `contato_id`) VALUES
(73, 'Doriene do nascimento guida', '54700230100', '12/12/1212', '17cf624a31776219c1897c1a44fb30c6678955b1c1e3151951147cf614563606c5e2e443345246cff0c256bd108f4286fcf68123829885a1acbeebd5cb0091e6', 'c85adea782af1415a020d6089c89a5e6fee8033a9808a0527a25bacf287c6656e65d6aaa6841d301f0b3d5f578d57bfce3df45518e1a8d2e80dcbe3db3d772c2', NULL, 'coletor', 1, '', 83),
(83, 'Luciano Guida Clemente', '05848058105', '08011996', 'ba0c1c936e377c632bde75af933110960eec311c08c4fd9dca2d7cc6ed105ae6032bd76170bd365f8d2950307bd2f3c0000f435f91511a3ba1654a6078efd584', 'de8bdfcb44faa770e4c8deaf223e06006c10e688ed0cb7050d6e2ac95b6ea2eb5298e02be562b7c5a6f6f4429327b25fe013a352d85a1de02b428b56a06f28a4', NULL, 'descartante', 1, '9e48ba262ed924830936a85de72441f3', 94);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos`
--
ALTER TABLE `contatos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `descartes`
--
ALTER TABLE `descartes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_descartes_users1_idx` (`user_id`),
  ADD KEY `fk_descartes_categorias1_idx` (`categoria_id`),
  ADD KEY `fk_descartes_enderecos1_idx` (`endereco_id`);

--
-- Indexes for table `enderecos`
--
ALTER TABLE `enderecos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_enderecos_states1_idx` (`cidade_id`),
  ADD KEY `fk_enderecos_users1_idx` (`user_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_attempts_user_id` (`user_id`);

--
-- Indexes for table `lost_passwords`
--
ALTER TABLE `lost_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lost_password_user_id_idx` (`user_id`);

--
-- Indexes for table `minhascategorias`
--
ALTER TABLE `minhascategorias`
  ADD PRIMARY KEY (`user_id`,`categoria_id`),
  ADD KEY `fk_users_has_categorias_categorias1_idx` (`categoria_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_contatos1_idx` (`contato_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=584;
--
-- AUTO_INCREMENT for table `contatos`
--
ALTER TABLE `contatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `descartes`
--
ALTER TABLE `descartes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `enderecos`
--
ALTER TABLE `enderecos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lost_passwords`
--
ALTER TABLE `lost_passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `descartes`
--
ALTER TABLE `descartes`
  ADD CONSTRAINT `fk_descartes_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_descartes_enderecos1` FOREIGN KEY (`endereco_id`) REFERENCES `enderecos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_descartes_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `enderecos`
--
ALTER TABLE `enderecos`
  ADD CONSTRAINT `fk_enderecos_states1` FOREIGN KEY (`cidade_id`) REFERENCES `cidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_enderecos_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD CONSTRAINT `login_attempts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `lost_passwords`
--
ALTER TABLE `lost_passwords`
  ADD CONSTRAINT `lost_password_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `minhascategorias`
--
ALTER TABLE `minhascategorias`
  ADD CONSTRAINT `fk_users_has_categorias_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_categorias_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_contatos1` FOREIGN KEY (`contato_id`) REFERENCES `contatos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
