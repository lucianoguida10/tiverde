<?php
class Descarte extends Model{

    /**
     * Configuração para a associação entre tabelas
     * @var array
     */
    static $belongs_to = array(
        array('user'),
        array('categoria'),
        array('endereco')
    );

    public static function cadastrar($material,$descricao,$img,$user_id,$categoria,$endereco_id){
        $categoria = Categoria::find_by_id($categoria);
        if(!is_null($categoria)){
            $dados = array(
                'material'=>$material,
                'descricao'=>$descricao,
                'img'=>$img,
                'status'=>1,
                'user_id'=>$user_id,
                'categoria_id'=>$categoria->id,
                'endereco_id'=>$endereco_id
            );
            if(self::create($dados)){
                return $data = array('valid'=>true,'dados' => self::last());
            }else{
                return $data = array(
                    'valid'=> false,
                    'msg'=>array('danger','Não foi possivel adicionar seu descarte!','Verifique os dados e tente novamente!!')
                );
            }
        }else{
            return $data = array(
                'valid'=> false,
                'msg'=>array('danger','Não foi possivel adicionar seu descarte!','Verifique os dados e tente novamente!!')
            );
        }
    }

    public static function meus(){
        $des = self::all(array('conditions'=>array('user_id = '.Auth::userActive()->id.' and status <> 3'),'order'=>'status ASC'));
        return $des;
    }
}