<?php
	class User extends Model{

		/**
		 * Configuração para a associação entre tabelas
		 * @var array
		 */
		static $belongs_to = array(
			array('cidade'),
            array('contato')
		);

		/**
		 * Configuração para a associação entre tabelas
		 * @var array
		 */
		static $has_many = array(
            array('enderecos'),
		 	array('login_attempts'),
			array('lost_passwords'),
            array('minhascategorias')
		);

		/**
		 * Método responsável por retornar um array com as IDs e nomes dos usuários
		 * @return array Array tratado para o PFBC
		 */
		public static function getOptions(){
			$users=self::all();
			$options=array(
				''=>'Selecione...'
			);
			foreach ($users as $user) {
				$options[$user->id]=$user->nome;
			}
			return $options;
		}

		/**
         * Metodo para cadastrar user
         */
		public static function cadastrar($post,$contao_id){
            foreach ($post as $key => $val){
                if($key == 'nome' or $key == 'cpf'){
                    $dados[$key] = $val;
                }
            }
            $credencial = Tools::hashHX($_REQUEST['data_nascimento']);
            $dados['password'] = $credencial['password'];;
            $dados['salt'] = $credencial['salt'];;
            $dados['role'] = 'descartante';
            $dados['status'] = 3;
            $dados['contato_id'] = $contao_id;
            $dados['data_nascimento'] = $_REQUEST['data_nascimento'];
            $dados['token'] = md5($_REQUEST['cpf'].'Guida');
            if(self::create($dados)){
                return $data = array('valid'=>true,'dados' => self::last());
            }else{
                return $data = array(
                    'valid'=> false,
                    'msg'=>array('Danger','Não foi possivel adicionar seu seu usuario.','Verifique seus dados e tente novamente!!')
                );
            }
        }
        public static function cadstrado($cpf){
		    $user = self::find_by_cpf($cpf);
		    if(is_null($user) && is_null($user)){
		        return true;
            }else{
		        return false;
            }
        }
	}