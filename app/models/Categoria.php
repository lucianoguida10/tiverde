<?php
	class Categoria extends Model{

		/**
		 * Configuração para a associação entre tabelas
		 * @var array
		 */
        static $has_many = array(
            array('descartes'),
            array('minhascategorias')
        );
        public static function verifica($id){
            /**
             * FUNÇÃO QUE VERIFICA SE O USUARIO LOGADO TEM ESSA CATEGORIA
             */
            $categoria = self::find_by_id($id);
            $user = Auth::userActive();
            if(!is_null($categoria)){
                $minhas = Minhascategoria::all(array('conditions'=>array('user_id = '.$user->id.' and categoria_id = '.$categoria->id)));
                if(!is_null($minhas)&& !empty($minhas)) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        public static function del($id){
            /**
             * FUNÇÃO DE DELETAR DE MINHAS CATEGORIAS
             */
            $categoria = self::find_by_id($id);
            $user = Auth::userActive();
            if(!is_null($categoria)){
                $minhas = Minhascategoria::all(array('conditions'=>array('user_id = '.$user->id.' and categoria_id = '.$categoria->id)));
                if(!is_null($minhas)&& !empty($minhas)) {
                    $minhas[0]->delete();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
	}