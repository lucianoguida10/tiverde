<?php
class Contato extends Model{

    /**
     * Configuração para a associação entre tabelas
     * @var array
     */
    static $has_many = array(
        array('users')
    );
    public static function cadastrar($post){
        foreach ($post as $key => $val){
           if($key == 'telefone' or $key == 'email'){
               $dados[$key] = $val;
           }
        }
        if(self::create($dados)){
            return $data = array('valid'=>true,'dados' => self::last());
        }else{
            return $data = array(
                'valid'=> false,
                'msg'=>array('Danger','Não foi possivel adicionar seu contato!','Verifique seus dados e tente novamente!!')
            );
        }
    }
}