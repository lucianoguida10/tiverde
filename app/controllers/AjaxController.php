<?php
class AjaxController extends Controller{
    public function LatitudeAction($adrss){

        $local = DataHelper::tratar($adrss,'sanitize');
        $cep = utf8_encode(str_replace(" ", "+", $local)); //echo $cep;
        $geocode = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$cep.'&key=AIzaSyDKzCzKX6g4Q3-9z4Mie9ZE_lavx8HHoAQ'));

        if(!empty($geocode->results)) {
            $lat = $geocode->results[0]->geometry->location->lat;
            echo $lat;
        }else{
            echo 0;
        }
    }
    public function LongitudeAction($adrss){
        //echo $adrss;
        $local = DataHelper::tratar($adrss,'sanitize');
        $cep = utf8_encode(str_replace(" ", "+", $local));
        $geocode = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$cep.'&key=AIzaSyDKzCzKX6g4Q3-9z4Mie9ZE_lavx8HHoAQ'));
        //echo $cep;
        if(!empty($geocode->results)) {
            $long = $geocode->results[0]->geometry->location->lng;
            //echo "<pre>";
            //var_dump($geocode);
            echo $long;
        }else{
            echo 0;
        }
    }
}