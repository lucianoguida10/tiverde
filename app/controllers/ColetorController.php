<?php
class ColetorController extends Controller{
    public function __construct(){
        parent::__construct();
        Auth::redirectCheck();
        Auth::role_check('coletor');
    }
    public function AtenderAction($id){
        $descarte = Descarte::find_by_id($id);
        if(!empty($descarte) && is_null($descarte->coletor_id) && $descarte->status == 1){
            $atendimento = Descarte::all(array('conditions'=>array('coletor_id = '.Auth::userActive()->id.' and status = 1 or status = 2')));
            if(empty($atendimento)){
                $descarte->coletor_id = Auth::userActive()->id;
                $descarte->save();
                $email = new EmailHelper();
                $email->enviar($descarte->user->contato->email,
                    $descarte->user->nome,
                    'T.I. Verde - Coleta',
                    '',
                    'atender',
                    null,
                    null,
                    $descarte);
                $this->view('atender', array('descarte' => $descarte));
            }else{
                $this->view('atender',array('descarte'=>$atendimento[0],
                    'message'=>array('info','Você não pode atender outo descarte ate finalizar este!','Caso já tenha coletado entre em contato com responsavel para finalizar a coletar.')));
            }
        }elseif($descarte->coletor_id == Auth::userActive()->id){
            $this->view('atender',array('descarte'=>$descarte));
        }else{
            $this->redirectTo(SITE.'home');
        }
    }
    public function CategoriasAction(){
        $data['categorias'] = Categoria::all();
        if(!empty($_REQUEST)&& !isset($_REQUEST['type'])){
            foreach ($data['categorias'] as $val){
                if(isset($_REQUEST[$val->id])){
                    if(!Categoria::verifica($val->id)) {
                        Minhascategoria::create(array('user_id' => Auth::userActive()->id, 'categoria_id' => $val->id));
                        $t= true;
                    }
                }else{
                    Categoria::del($val->id);
                    $t = true;
                }
            }
            if($t){
                $data['message'] = array('success','Suas categoras de atendimento foram atualizadas!');
            }
        }elseif (isset($_REQUEST['type']) && !empty($_REQUEST['categoria'])){
            Categoria::create(array('nome'=>$_REQUEST['categoria']));
            Minhascategoria::create(array(
                'user_id'=>Auth::userActive()->id,
                'categoria_id'=>Categoria::last()->id
            ));
            $data['message'] = array('success',
                'Categoria enserida com sucesso!',
                'Obs: Esta categoria inserida sera disponibilizada para todos os coletores.');
            $data['categorias'] = Categoria::all();
        }
        $this->view('categoriasInteresse',$data);
    }
}