<?php
	class HomeController extends Controller{
		public function __construct(){
			parent::__construct();
			Auth::redirectCheck();
		}
		public function indexAction(){
		    $data['descartes'] = Descarte::all(array('conditions'=>array('user_id <> '.Auth::userActive()->id.' and status = 1 and coletor_id is null')));
            $minhasc = Minhascategoria::all(array('conditions'=>array('user_id = '.Auth::userActive()->id)));
            if(!empty($minhasc)) {
                foreach ($minhasc as $val) {
                    $minhas[$val->categoria_id] = $val->categoria_id;
                }
                ## organiza em primeiro as categoria de interesses.
                foreach ($data['descartes'] as $val) {
                    if (in_array($val->categoria_id, $minhas)) {
                        $data['descartei'][$val->id] = $val;
                    } else {
                        $data['descartess'][$val->id] = $val;
                    }
                }
                unset($data['descartes']);
            }else{
                $data['descartess'] = $data['descartes'];
                unset($data['descartes']);
            }
            #var_dump($data);
		    $data['meusdescartes'] = Descarte::meus();
		    $this->view('Home',$data);
		}
	}