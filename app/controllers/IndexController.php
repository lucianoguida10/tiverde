<?php 
    class IndexController extends Controller{
        public function indexAction(){
            $data["categoria"] = Categoria::all();
            $this->view('Index',$data,true,'Generic','Generic',array(
            	CSS.'animate.css'
            ));
        }
        public function verificarAction($token){
            $user = User::find_by_token($token);
            if(!empty($user)){
                $user->status = 1;
                $user->save();
                $data['message'] = array('success','Email verificado com sucesso!');
            }else{
                $data['message'] = array('danger','Não identificamos o seu cadastro!','Favor verifique o email enviado para você e tente novamente!');
            }
            $this->view('Login',$data,true,'Generic','Generic');
        }
    }