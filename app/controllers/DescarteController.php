<?php
class DescarteController extends Controller{
    public function pcadastroAction($msg = ""){
        if($msg != ""){ $data['message'] = $msg; }else{ $data = array();}
        $data['preCadastro'] = ($_REQUEST);
        $data["categoria"] = Categoria::all();
        $this->view("cadDescarte",$data,true,'generic','Generic');
    }
    public function cadastrarAction(){
        $validetor = new Validator($_REQUEST);
        $_REQUEST['cpf'] = preg_replace("/[^0-9]/", "", $_REQUEST['cpf']);
        $_REQUEST['data_nascimento'] = preg_replace("/[^0-9]/", "", $_REQUEST['data_nascimento']);
        $validetor->field_filledIn($_REQUEST);
        $validetor->field_email("email");
        $validetor->field_cadastropessoa("cpf");
        $local = DataHelper::tratar($_REQUEST['cep'].'+'.$_REQUEST['cidade'],'sanitize');
        $cep = utf8_encode(str_replace(" ", "+", $local));
        $geocode = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$cep.'&key=AIzaSyDKzCzKX6g4Q3-9z4Mie9ZE_lavx8HHoAQ'));
        if(!empty($geocode->results)) {
            $lat = $geocode->results[0]->geometry->location->lat;
            $long = $geocode->results[0]->geometry->location->lng;
            if($validetor->valid){
                if(User::cadstrado($_REQUEST)) {
                    $contato = Contato::cadastrar($_REQUEST);
                    if ($contato['valid']) {
                        $contato = $contato['dados'];
                        $user = User::cadastrar($_REQUEST, $contato->id);
                        if ($user['valid']) {
                            $user = $user['dados'];
                            $endereco = Endereco::cadastrar($_REQUEST['cidade'],$lat,$long,$_REQUEST['cep'] ,$user->id);
                            if ($endereco['valid']) {
                                $endereco = $endereco['dados'];
                                # ** EMVIO DA IMAGEM
                                if (isset($_FILES['img'])) {
                                    $ext = strtolower(substr($_FILES['img']['name'], -4)); //Pegando extensão do arquivo
                                    $extensoes = array('.gif', '.jpeg', '.jpg', '.png');
                                    if (in_array($ext, $extensoes)) {
                                        $new_name = md5(date("Y.m.d-H.i.s")) . $ext; //Definindo um novo nome para o arquivo
                                        $dir = "public/img/upload/"; //Diretório para uploads
                                        $descarte = Descarte::cadastrar($_REQUEST['material'], $_REQUEST['descricao'], "upload/" . $new_name, $user->id, $_REQUEST['categoria'], $endereco->id);
                                        if ($descarte['valid']) {
                                            move_uploaded_file($_FILES['img']['tmp_name'], $dir . $new_name); //Fazer upload do arquivo
                                            $email = new EmailHelper();
                                            $email->enviar($contato->email,
                                                $user->nome,
                                                'Verifique seu email - T.I. Verde',
                                                'Você se cadastrou na T.I. Verde - A melhor comunidade de descarte e colata!<br>Clique no botão abaixo e verifique seu email.',
                                                'email',
                                                SITE.'index/verificar/'.$user->token
                                            );
                                            $this->redirectTo(SITE . 'login/');
                                        } else {
                                            $endereco->delete();
                                            $user->delete();
                                            $contato->delete();
                                            $this->pcadastroAction($descarte['msg']);
                                        }
                                    } else {
                                        $endereco->delete();
                                        $user->delete();
                                        $contato->delete();
                                        $this->pcadastroAction(array(
                                            'danger',
                                            'Ops! a imagem fora da extenções determinadas.',
                                            "Tipos de imagens aceitas .gif, .jpeg, .jpg, .png"
                                        ));
                                    }
                                }
                            } else {
                                $user->delete();
                                $contato->delete();
                                $this->pcadastroAction($endereco['msg']);
                            }
                        } else {
                            $contato->delete();
                            $this->pcadastroAction($user['msg']);
                        }
                    } else {
                        $contato->delete();
                        $this->pcadastroAction($contato['msg']);
                    }
                }else{
                    $this->pcadastroAction(array(
                        'info',
                        'CPF já cadastrado!',
                        '<a href="'.SITE.'login">Clique aqui</a> e faça login para realizar seu descarte!'
                    ));
                }
            }else{
                $this->pcadastroAction($validetor->getErrors());
            }
        }else{
            $this->pcadastroAction(array(
                'danger',
                'CEP invalido',
                'Não conseguimos obter informação sobre seu cep.'
            ));
        }
    }
    public function alterarAction($id,$msg = ""){
        if($msg != ""){ $data['message'] = $msg; }else{ $data = array();}
        Auth::redirectCheck();
        $descarte = Descarte::find_by_id($id);
        if(!is_null($descarte) && $descarte->user_id == Auth::userActive()->id) {
            $data["categoria"] = Categoria::all();
            //$data['cidades'] = Cidade::all();
            $data['preCadastro']['categoria'] =$descarte->categoria->id;
            $data['preCadastro']['material'] = $descarte->material;
            $data['preCadastro']['descricao'] = $descarte->descricao;
            $data['preCadastro']['id'] = $descarte->id;

            if (isset($_REQUEST['categoria'])) {
                $validetor = new Validator($_REQUEST);
                $validetor->field_filledIn($_REQUEST);
                if (!empty($_FILES['img']['name']) && $validetor->valid) {
                    $ext = strtolower(substr($_FILES['img']['name'], -4)); //Pegando extensão do arquivo
                    $extensoes = array('.gif', '.jpeg', '.jpg', '.png');
                    if (in_array($ext, $extensoes)) {
                        $new_name = md5(date("Y.m.d-H.i.s")) . $ext; //Definindo um novo nome para o arquivo
                        $dir = "public/img/upload/"; //Diretório para uploads
                        move_uploaded_file($_FILES['img']['tmp_name'], $dir . $new_name); //Fazer upload do arquivo
                        unlink('public/img/'.$descarte->img);
                        $descarte->categoria_id = $_REQUEST['categoria'];
                        $descarte->material = $_REQUEST['material'];
                        $descarte->descricao = $_REQUEST['descricao'];
                        $descarte->img = "upload/".$new_name;
                        $descarte->save();
                        $_REQUEST = null;
                        $this->alterarAction($descarte->id,array(
                            'success',
                            'Alteração salva com sucesso!'
                        ));
                    }else{
                        $_REQUEST = null;
                        $this->alterarAction($descarte->id,array(
                            'danger',
                            'Ops! a imagem esta fora da extenções determinadas.',
                            "Tipos de imagens aceitas .gif, .jpeg, .jpg, .png"
                        ));
                    }
                } elseif($validetor->valid) {
                    $descarte->categoria_id = (int) $_REQUEST['categoria'];
                    $descarte->material = $_REQUEST['material'];
                    $descarte->descricao = $_REQUEST['descricao'];
                    $descarte->save();
                    $_REQUEST = null;
                    $this->alterarAction($descarte->id,array(
                        'success',
                        'Alteração salva com sucesso!'
                    ));
                }else{
                    $_REQUEST = null;
                    $this->alterarAction($descarte->id,$validetor->getErrors());
                }
            } else {
                $this->view('editarDescarte', $data);
            }
        }else{
            $this->redirectTo(SITE.'home');
        }
    }
    public function criarAction($msg = ''){
        if($msg != ""){ $data['message'] = $msg; }else{ $data = array();}
        Auth::redirectCheck();
        $data["categoria"] = Categoria::all();
        //$data['cidades'] = Cidade::all();
        if (isset($_REQUEST['categoria']) && isset($_REQUEST['material'])) {
            $validetor = new Validator($_REQUEST);
            $validetor->field_filledIn($_REQUEST);
            if ($validetor->valid) {
                $ext = strtolower(substr($_FILES['img']['name'], -4)); //Pegando extensão do arquivo
                $extensoes = array('.gif', '.jpeg', '.jpg', '.png');
                if (in_array($ext, $extensoes)) {
                    $new_name = md5(date("Y.m.d-H.i.s")) . $ext; //Definindo um novo nome para o arquivo
                    $dir = "public/img/upload/"; //Diretório para uploads
                    move_uploaded_file($_FILES['img']['tmp_name'], $dir . $new_name); //Fazer upload do arquivo
                    $descarte = Descarte::cadastrar($_REQUEST['material'],$_REQUEST['descricao'],'upload/'.$new_name,Auth::userActive()->id,$_REQUEST['categoria'],Auth::userActive()->enderecos[0]->id);
                    $_REQUEST = null;
                    if($descarte['valid']){
                        $email = new EmailHelper();
                        $user  = Auth::userActive();
                        $email->enviar($user->contato->email,
                            $user->nome,
                            'T.I. Verde - Cadstro de descarte',
                            '',
                            'descarte',
                            null,
                            null,
                            $descarte['dados']);
                        $this->criarAction(array(
                            'success',
                            'Descarte cadastrado com sucesso!',
                            'Descarte n° '.$descarte['dados']->id
                        ));
                    }
                }else{
                    $_REQUEST = null;
                    $this->criarAction(array(
                        'danger',
                        'Ops! a imagem fora da extenções determinadas.',
                        "Tipos de imagens aceitas .gif, .jpeg, .jpg, .png"
                    ));
                }
            }else{
                $_REQUEST = null;
                $this->criarAction($validetor->getErrors());
            }
        } else {
            $this->view('criarDescarte', $data);
        }
    }
    public function deleteAction($id){
        Auth::redirectCheck();
        $descarte = Descarte::find_by_id($id);
        if(!is_null($descarte) && $descarte->user_id == Auth::userActive()->id && is_null($descarte->coletor_id)) {
            $descarte->status = 2;
            $descarte->save();
        }
        $this->redirectTo(SITE.'home');
    }
    public function finalizarAction($id){
        Auth::redirectCheck();
        $descarte = Descarte::find_by_id($id);
        if(!is_null($descarte) && $descarte->user_id == Auth::userActive()->id && !is_null($descarte->coletor_id)) {
            $descarte->status = 3;
            $email = new EmailHelper();
            $usdescarte = User::find($descarte->coletor_id);
            $email->enviar($usdescarte->contato->email,
                $usdescarte->nome,
                'T.I. Verde - Finalizar coleta',
                '',
                'finalizar',
                null,
                null,
                $descarte);
            var_dump($email);
            $descarte->save();
        }
        $this->redirectTo(SITE.'home');
    }
    public function ativarAction($id){
        Auth::redirectCheck();
        $descarte = Descarte::find_by_id($id);
        if(!is_null($descarte) && $descarte->user_id == Auth::userActive()->id && is_null($descarte->coletor_id)) {
            $descarte->status = 1;
            $descarte->save();
        }
        $this->redirectTo(SITE.'home');
    }
}