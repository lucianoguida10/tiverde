<?php

class UsuariosController extends Controller{
    public function __construct(){
        parent::__construct();
        Auth::redirectCheck();
    }
    public function indexAction(){
        #$this->view('Login','',true,'Generic','Generic',array(CSS.'animate.css'));
    }
    public function editarAction($id,$msg = ''){
        if($msg != ""){ $data['message'] = $msg; }else{ $data = array();}
        $data["categoria"] = Categoria::all();
       // $data['cidades'] = Cidade::all();
        $user = Auth::userActive();

        $data['preCadastro']['nome'] = $user->nome;
        $data['preCadastro']['email'] = $user->contato->email;
        $data['preCadastro']['telefone'] = $user->contato->telefone;
        $data['preCadastro']['cpf'] = $user->cpf;
        $data['preCadastro']['data_nascimento'] = date("d/m/Y", strtotime($user->data_nascimento));
        $data['preCadastro']['cidade'] = $user->enderecos[0]->cidade;
        $data['preCadastro']['CEP'] = $user->enderecos[0]->cep;
        $data['preCadastro']['bairro'] = $user->enderecos[0]->bairro;
        $data['preCadastro']['rua'] = $user->enderecos[0]->rua;
        $data['preCadastro']['numero'] = $user->enderecos[0]->numero;
        $data['preCadastro']['principal'] = $user->enderecos[0]->principal;
        $data['preCadastro']['lng'] = $user->enderecos[0]->lng;
        $data['preCadastro']['lat'] = $user->enderecos[0]->lat;
        if(isset($_REQUEST['send'])){
            $validetor = new Validator($_REQUEST);
            $validetor->field_filledIn($_REQUEST);
            $validetor->field_email('email');
            //$cidade = Cidade::find_by_nome($_REQUEST['cidade']);
            if($validetor->valid){
                $user->nome = $_REQUEST['nome'];
                $user->contato->email = $_REQUEST['email'];
                $user->contato->telefone = $_REQUEST['telefone'];
                $user->data_nascimento = $_REQUEST['data_nascimento'];
                $user->enderecos[0]->cidade = $_REQUEST['cidade'];
                $user->enderecos[0]->bairro = isset($_REQUEST['bairro'])?$_REQUEST['bairro']:NULL;
                $user->enderecos[0]->rua = isset($_REQUEST['rua'])?$_REQUEST['rua']:NULL;
                $user->enderecos[0]->numero = isset($_REQUEST['numero'])?$_REQUEST['numero']:NULL;
                $user->enderecos[0]->cep = $_REQUEST['cep'];
                $user->enderecos[0]->lat = $_REQUEST['lat'];
                $user->enderecos[0]->lng = $_REQUEST['lng'];
                if(isset($_REQUEST['principal'])){$user->enderecos[0]->principal = true;}else{$user->enderecos[0]->principal = false;}
                $user->enderecos[0]->save();
                $user->save();
                $_REQUEST = null;
                $this->editarAction($user->id,array(
                    'success',
                    'Alterações salvas com sucesso!',
                ));
            }else{
                $_REQUEST = null;
                $this->editarAction($user->id,array('danger','Cidade invalida!','A cidade que você escolheu e invalida!'));
            }
        }else{
            $this->view('formUsuario',$data);
        }
    }
    public function sejacoletorAction($msg = ''){
        if($msg != ""){ $data['message'] = $msg; }else{ $data = array();}
        $data["categoria"] = Categoria::all();
        //$data['cidades'] = Cidade::all();
        $user = Auth::userActive();
        $data['preCadastro']['nome'] = $user->nome;
        $data['preCadastro']['email'] = $user->contato->email;
        $data['preCadastro']['telefone'] = $user->contato->telefone;
        $data['preCadastro']['cpf'] = $user->cpf;
        $data['preCadastro']['data_nascimento'] = date("d/m/Y", strtotime($user->data_nascimento));
        $data['preCadastro']['cidade'] = $user->enderecos[0]->cidade;
        $data['preCadastro']['CEP'] = $user->enderecos[0]->cep;
        $data['preCadastro']['bairro'] = $user->enderecos[0]->bairro;
        $data['preCadastro']['rua'] = $user->enderecos[0]->rua;
        $data['preCadastro']['numero'] = $user->enderecos[0]->numero;
        $data['preCadastro']['principal'] = $user->enderecos[0]->principal;
        $data['preCadastro']['lng'] = $user->enderecos[0]->lng;
        $data['preCadastro']['lat'] = $user->enderecos[0]->lat;
        if(isset($_REQUEST['send'])){
            $validetor = new Validator($_REQUEST);
            $validetor->field_filledIn($_REQUEST);
            $validetor->field_email('email');
            //$cidade = Cidade::find_by_nome($_REQUEST['cidade']);
            if($validetor->valid){
                $user->nome = $_REQUEST['nome'];
                $user->contato->email = $_REQUEST['email'];
                $user->contato->telefone = $_REQUEST['telefone'];
                $user->data_nascimento = $_REQUEST['data_nascimento'];
                $user->enderecos[0]->cidade = $_REQUEST['cidade'];
                $user->enderecos[0]->bairro = $_REQUEST['bairro'];
                $user->enderecos[0]->rua = $_REQUEST['rua'];
                $user->enderecos[0]->numero = $_REQUEST['numero'];
                $user->enderecos[0]->cep = $_REQUEST['cep'];
                $user->enderecos[0]->lat = $_REQUEST['lat'];
                $user->enderecos[0]->lng = $_REQUEST['lng'];
                $user->role = 'coletor';
                if(isset($_REQUEST['principal'])){$user->enderecos[0]->principal = true;}else{$user->enderecos[0]->principal = false;}
                $user->enderecos[0]->save();
                $user->save();
                $_REQUEST = null;
                $this->sejacoletorAction(array(
                    'success',
                    'Alterações salvas com sucesso!',
                    'Agora você é um coletor vá a pagina inicial e verifique se ha produtos do meu enteresse!'
                ));
            }else {
                $this->sejacoletorAction($validetor->getErrors());
            }
        }else{
            $this->view('SejaColetor',$data);
        }
    }
}