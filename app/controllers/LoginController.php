<?php 
    class LoginController extends Controller{
    	public function __construct(){
    		parent::__construct();
    		Auth::redirectCheck(true);
    	}
        public function indexAction(){
            $this->view('Login','',true,'Generic','Generic',array(CSS.'animate.css'));
        }
        public function logarAction(){
    		$data=array();
    		//Validação
            $_REQUEST['cpf'] = preg_replace("/[^0-9]/", "", $_REQUEST['cpf']);
            $_REQUEST['data_nascimento'] = preg_replace("/[^0-9]/", "", $_REQUEST['data_nascimento']);
    		$validator=new Validator($_REQUEST);
    		$validator->field_filledIn();
            $validator->field_cadastropessoa('cpf');
    		if(!$validator->valid){
    			$data["message"]=$validator->getErrors();
    		}else{
    			//Tratamento
    			//$super_global=DataHelper::tratamento($_REQUEST,INPUT_POST);

    			//Autenticação
    			$auth=new Auth;
    			$auth->login($_REQUEST["cpf"],$_REQUEST["data_nascimento"]);
    			if($auth->check()){
    				$this->redirectTo(SITE."home");
    			}else{
    				$data["message"]=$auth->getErrors();
    			}
    		}
    		$this->view('Login',$data,true,'Generic','Generic');
        }
        public function sairAction(){
        	Auth::logout();
        }
    }