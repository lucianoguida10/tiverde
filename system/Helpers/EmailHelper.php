<?php

use PHPMailer\PHPMailer\PHPMailer;

class EmailHelper{
		
		/**
		 * Método responsável pelo envio de emails
		 * @param  string $email    E-mail para qual será enviada a mensagem
		 * @param  string $assunto  Assunto da mensagem
		 * @param  string $mensagem Mensagem
		 * @param  array  $config   Array com Remetente e E-mail do remetente
		 * @return array            Array com o status de envio e mensagem
		 */
		protected function htmlEmail($mensagem,$nome=null,$token=null){
            $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>T.I. Verde</title><meta name="viewport" content="width=device-width, initial-scale=1.0"/><style type="text/css">.myButton {background-color:#44c767;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:10px 40px;text-decoration:none;}.myButton:hover {background-color:#5cbf2a;}.myButton:active {position:relative;top:1px;}</style></head><body style="margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border-radius: 50px;"><tr><td style="max-width: 600px;height: 300px; position: relative; overflow: hidden;"><img src="https://partnersintheparks.org/wp-content/uploads/2019/02/adventure-camping-daylight-1758303.jpg" alt="" style="position:absolute; width: 600px; margin-top: -150px;" /><h2 style="position: absolute; top: 0px; font-size: 45pt; font-family: arial;left: 120px;color: green">T.I. VEREDE</h2></td></tr><tr><td bgcolor="#ffffff" style="text-align: justify;font-family: arial;box-shadow: 0px -73px 76px -24px rgba(0,0,0,0.75); padding: 50px">
                        Olá, <b>'.$nome.'</b> Tudo bem ?<p>'.$mensagem.'</p><a href="'.$token.'" class="myButton">Confirmar</a></td></tr><tr><td bgcolor="black" style="color: white;text-align: center;padding: 15px">T.I. VERDE v1.0 2019 © - Todos os direitos reservados</td></tr></table></td></tr></table></body></html>';
            return $html;
		}
		protected function htmlCaddescarte($nome,$descarte){
		    return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>T.I. Verde</title><meta name="viewport" content="width=device-width, initial-scale=1.0"/><style type="text/css">.myButton {background-color:#44c767;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:10px 40px;text-decoration:none;}.myButton:hover {background-color:#5cbf2a;}.myButton:active {position:relative;top:1px;}</style></head><body style="margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border-radius: 50px;"><tr><td style="max-width: 600px;height: 300px; position: relative; overflow: hidden;"><img src="https://partnersintheparks.org/wp-content/uploads/2019/02/adventure-camping-daylight-1758303.jpg" alt="" style="position:absolute; width: 600px; margin-top: -150px;" /><h2 style="position: absolute; top: 0px; font-size: 45pt; font-family: arial;left: 120px;color: green">T.I. VEREDE</h2></td></tr><tr>
	<td bgcolor="#ffffff" style="text-align: justify;font-family: arial;box-shadow: 0px -73px 76px -24px rgba(0,0,0,0.75); padding: 50px">
		Olá, <b>'.$nome.'</b> !
		<br><br>Seu descarte com a categoria <b>'.$descarte->categoria->nome.'</b> foi cadastrado com sucesso!
		<br><br>Logo algum alguem ira coleta-lo.
    </td></tr><tr><td bgcolor="black" style="color: white;text-align: center;padding: 15px">T.I. VERDE v1.0 2019 © - Todos os direitos reservados</td></tr></table></td></tr></table></body></html>';
        }
        protected function htmlAtender($nome,$descarte){
		    $coletor = User::find_by_id($descarte->coletor_id);
            return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>T.I. Verde</title><meta name="viewport" content="width=device-width, initial-scale=1.0"/><style type="text/css">.myButton {background-color:#44c767;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:10px 40px;text-decoration:none;}.myButton:hover {background-color:#5cbf2a;}.myButton:active {position:relative;top:1px;}</style></head><body style="margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border-radius: 50px;"><tr><td style="max-width: 600px;height: 300px; position: relative; overflow: hidden;"><img src="https://partnersintheparks.org/wp-content/uploads/2019/02/adventure-camping-daylight-1758303.jpg" alt="" style="position:absolute; width: 600px; margin-top: -150px;" /><h2 style="position: absolute; top: 0px; font-size: 45pt; font-family: arial;left: 120px;color: green">T.I. VEREDE</h2></td></tr><tr>
        <td bgcolor="#ffffff" style="text-align: justify;font-family: arial;box-shadow: 0px -73px 76px -24px rgba(0,0,0,0.75); padding: 50px">
            Olá, <b>'.$nome.'</b> !
            <br><br>O coletor <b>'.$coletor->nome.'</b> se interessou por seu porduto descartado! entre em contato pelo telefone: '.$coletor->contato->telefone.'
            <br><br>Descarte n° '.$descarte->id.'
            <br>Categoria: '.$descarte->categoria->nome.'
            <br>Descrição: '.$descarte->descricao.'
        </td></tr><tr><td bgcolor="black" style="color: white;text-align: center;padding: 15px">T.I. VERDE v1.0 2019 © - Todos os direitos reservados</td></tr></table></td></tr></table></body></html>';
        }
        protected function htmlFinalizar($nome,$descarte){
            return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>T.I. Verde</title><meta name="viewport" content="width=device-width, initial-scale=1.0"/><style type="text/css">.myButton {background-color:#44c767;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:10px 40px;text-decoration:none;}.myButton:hover {background-color:#5cbf2a;}.myButton:active {position:relative;top:1px;}</style></head><body style="margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border-radius: 50px;"><tr><td style="max-width: 600px;height: 300px; position: relative; overflow: hidden;"><img src="https://partnersintheparks.org/wp-content/uploads/2019/02/adventure-camping-daylight-1758303.jpg" alt="" style="position:absolute; width: 600px; margin-top: -150px;" /><h2 style="position: absolute; top: 0px; font-size: 45pt; font-family: arial;left: 120px;color: green">T.I. VEREDE</h2></td></tr><tr>
            <td bgcolor="#ffffff" style="text-align: justify;font-family: arial;box-shadow: 0px -73px 76px -24px rgba(0,0,0,0.75); padding: 50px">
                Olá, <b>'.$nome.'</b> !
                <br><br>O descartante <b>'.$descarte->user->nome.'</b> finalizou sua coleta. Agora você pode atentder outro descarte!
                <br/><br/><h4>Parabéns, continue coletando e ajudando o meio ambiente!</h4>
            </td></tr><tr><td bgcolor="black" style="color: white;text-align: center;padding: 15px">T.I. VERDE v1.0 2019 © - Todos os direitos reservados</td></tr></table></td></tr></table></body></html>';
        }
		public function enviar($para,$nome,$assunto,$mensagem,$tipo,$token=null,$emailTxto=null,$descarte=null){
            $mail = new PHPMailer();;
            $mail->IsSMTP();
            $mail->CharSet = 'UTF-8';
            #$mail->True;
            $mail->Host = "smtp.gmail.com"; // Servidor SMTP
            $mail->SMTPSecure = "tls"; // conexão segura com TLS
            $mail->Port = 587;
            $mail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
            $mail->Username = "tiverde54@gmail.com"; // SMTP username
            $mail->Password = "T@iV3d3159"; // SMTP password
            $mail->From = "tiverde@gmail.com"; // From
            $mail->FromName = "T.I. Verde" ; // Nome de quem envia o email
            $mail->AddAddress($para, $nome); // Email e nome de quem receberá //Responder
            $mail->WordWrap = 50; // Definir quebra de linha
            $mail->Subject = $assunto ; // Assunto
            if($tipo == 'email') {
                $mail->Body = html_entity_decode($this->htmlEmail($mensagem,$nome, $token)); //Corpo da mensagem caso seja HTML
            }else if($tipo == 'descarte'){
                $mail->Body = html_entity_decode($this->htmlCaddescarte($nome,$descarte)); //Corpo da mensagem caso seja HTML
            }else if($tipo == 'atender'){
                $mail->Body = html_entity_decode($this->htmlAtender($nome,$descarte)); //Corpo da mensagem caso seja HTML
            }else if($tipo == 'finalizar'){
                $mail->Body = html_entity_decode($this->htmlFinalizar($nome,$descarte)); //Corpo da mensagem caso seja HTML
            }
            $mail->AltBody = $emailTxto ; //PlainText, para caso quem receber o email não aceite o corpo HTML
            $mail->IsHTML(true); // Enviar como HTML
            if(!$mail->Send()){
                return false;
            }else{
                return true;
            }
		}
	}