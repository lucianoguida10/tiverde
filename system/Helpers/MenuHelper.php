<?php
	class MenuHelper{

		/**
		 * Método responsável por definir o Array com menus e submenus
		 * @param  string $role Role do usuário
		 * @return array  $menu Array com os menus
		 */
		public static function menus($role){
			switch ($role) {
				case 'coletor':
					$menu=array(
						"Categorias de interesse/users"=>"coletor/categorias",
                        "Descartes/briefcase"=>array(
                            "Criar Novo"=>"descarte/criar/"
                        )
					);
					break;
				case 'descartante':
					$menu=array(
						"Descartes/briefcase"=>array(
						    "Criar Novo"=>"descarte/criar/"
                        )
					);
					break;
			}
			return $menu;
		}
	
		/**
		 * Método responsável por renderizar o menu em HTML
		 * @param  object $user       Usuário logado
		 * @param  string $controller Controller atual
		 * @return string $html       HTML do menu
		 */
		public static function render($user,$controller){
			if(is_null($user))
				return;
			$menus=self::menus($user->role);
			$controller=str_replace("Controller", "", $controller);
			$html='';
			foreach ($menus as $key => $value) {
				$explode=explode("/",$key);
				$title=$explode[0];
				$icon=$explode[1];
				if(is_array($value)){
					$values=array_values($value);
					$check=explode("/",$values[0]);
					$html.='
					    <li class="nav-item dropdown '.(($check[0] == $controller) ? 'active' : '').'">
					      <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-'.$icon.'"></i> <span>'.$title.'</span></a>
					      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					';
					foreach($value as $titulo => $link){
						$html.='<a class="dropdown-item" href="'.SITE.$link.'">'.$titulo.'</a>';
					}
					$html.='</li>';
				}else{
					$html.='<li '.((strpos($value, $controller) !== false) ? 'class="nav-item active"' : 'class="nav-item"').'><a class="nav-link" href="'.SITE.$value.'"><i class="fa fa-'.$icon.'"></i> <span>'.$title.'</span></a></li>';
				}
			}
			return $html;
		}
	}