<?php
	ini_set('display_errors', 1); 
	date_default_timezone_set('America/Sao_Paulo');
	ob_start();

	//Inclusão de arquivos de configuração
	require_once("system/Bootstrap/database.php");
	require_once("system/Bootstrap/constants.php");
	require_once("system/Bootstrap/autoloader.php");

	//Start da sessão
	Tools::sec_session_start();
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
	require_once('system/Modules/ActiveRecord/ActiveRecord.php');
    require_once('system/Modules/phpmailer/src/Exception.php');
    require_once('system/Modules/phpmailer/src/PHPMailer.php');
    require_once('system/Modules/phpmailer/src/SMTP.php');

	$app = new App();
	$app->run();